// Define the ItemProvider schema
interface ItemProvider {
  name: string;
  category: string;
  price: number;
  offerSale: boolean;
  description: string;
}

// Define the Item schema, with a reference to ItemProvider
interface Item {
  name: string;
  quantity: number;
  stock: number;
  provider: ItemProvider;
}

// Define the Location schema
interface Location {
  name: string;
  items: Item[];
}

// Define the Inventory schema
interface Inventory {
  locations: Location[];
}

// Define the Realm schema
const realmSchema = [Item, ItemProvider, Location, Inventory];

// Define the Realm configuration
const realmConfig = {
  schema: realmSchema,
  schemaVersion: 1,
};

// Define the Inventory app
const InventoryApp = () => {
  const [realm, setRealm] = useState<Realm | null>(null);
  const [inventory, setInventory] = useState<Inventory | null>(null);

  // Initialize the Realm database
  useEffect(() => {
    const initRealm = async () => {
      const newRealm = await Realm.open(realmConfig);
      setRealm(newRealm);
    };
    initRealm();
  }, []);

  // Load the inventory from the Realm database
  useEffect(() => {
    if (realm) {
      const inventory = realm.objects<Inventory>('Inventory')[0];
      setInventory(inventory);
    }
  }, [realm]);

  // Add an item to a location
  const addItem = (
    locationName: string,
    itemName: string,
    providerName: string,
    providerCategory: string,
    providerPrice: number,
    providerOfferSale: boolean,
    providerDescription: string,
    quantity: number
  ) => {
    if (realm && inventory) {
      realm.write(() => {
        const location = inventory.locations.find((loc) => loc.name === locationName);
        if (location) {
          let item = location.items.find((item) => item.name === itemName);
          if (item) {
            item.quantity += quantity;
            item.stock += quantity;
          } else {
            const provider = realm
              .objects<ItemProvider>('ItemProvider')
              .filtered(`name = "${providerName}"`)[0];
            if (provider) {
              location.items.push({ name: itemName, quantity, stock: quantity, provider });
            } else {
              location.items.push({
                name: itemName,
                quantity,
                stock: quantity,
                provider: {
                  name: providerName,
                  category: providerCategory,
                  price: providerPrice,
                  offerSale: providerOfferSale,
                  description: providerDescription,
                },
              });
            }
          }
        }
      });
    }
  };

  // Remove an item from a location
  const removeItem = (locationName: string, itemName: string, quantity: number) => {
    if (realm && inventory) {
      realm.write(() => {
        const location = inventory.locations.find((loc) => loc.name === locationName);
        if (location) {
          const itemIndex = location.items.findIndex((item) => item.name === itemName);
          if (itemIndex !== -1) {
            const item = location.items[itemIndex];
            if (item.quantity <= quantity) {
              location.items.splice(itemIndex, 1);
            } else {
              item.quantity -= quantity;
              item.stock -= quantity;
            }
          }
        }
      });
    }
  };

  return (
    <div>
      <h1>Inventory App</h1>
      {inventory && (
        <div>
          {inventory.locations.map((location) => (
            <div key={location.name}>
              <h2>{location.name}</h2>
              <ul>
                {location.items.map((item) => (
                  <li key={item.name}>
                    {item.name}: {item.quantity}/{item.stock}
                    <button onClick={() => addItem(location.name, item.name, 1)}>+</button>
                    <button onClick={() => removeItem(location.name, item.name, 1)}>-</button>
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default InventoryApp;